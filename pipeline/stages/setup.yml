---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.setup-prepare: |
  # Prepare the testing environment
  # Handle architecture naming differences between test environment and kernel
  export KPET_ARCH="${KPET_ARCH:-${ARCH_CONFIG}}"

  # Create checkout and build KCIDB entries for kernels that are part of the installed compose
  if is_true "${skip_kernel_installation}" ; then
    save_kcidb_for_prebuilt_kernels
  fi

.setup-download-bot-artifacts: |
  # ⬇ Download artifacts from previous jobs (if only testing the testing).
  artifact_suffix="${ARCH_CONFIG}"
  # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
  if is_true "${DEBUG_KERNEL}"; then
    artifact_suffix="${artifact_suffix}_debug"
  fi
  ARTIFACT_URL_VAR="ARTIFACT_URL_${artifact_suffix}"
  ARTIFACT_JOB_NAME_VAR="ARTIFACT_JOB_NAME_${artifact_suffix}"
  ARTIFACT_JOB_ID_VAR="ARTIFACT_JOB_ID_${artifact_suffix}"
  if [ -n "${!ARTIFACT_URL_VAR:-}" ]; then
    # artifacts meta file should come from previous pipeline
    rm -f "${ARTIFACTS_META_PATH}"
    if ! curl --config "${CKI_CURL_CONFIG_FILE}" --output artifacts.zip "${!ARTIFACT_URL_VAR}?job_token=${CI_JOB_TOKEN}" || ! unzip -o artifacts.zip; then
      cki_echo_notify "  GitLab artifacts expired, continuing with current S3 artifact settings."
    fi
    SOURCE_PATH="${ARTIFACT_PIPELINE_ID}/${!ARTIFACT_JOB_NAME_VAR}/${!ARTIFACT_JOB_ID_VAR}"
    if [[ -f "${ARTIFACTS_META_PATH}" ]]; then
      S3_TARGET_PATH=$(jq -r '.s3_target_path // ""' < "${ARTIFACTS_META_PATH}")
      if [[ -n ${S3_TARGET_PATH} ]]; then
        SOURCE_PATH=${S3_TARGET_PATH}
      fi
    fi
    aws_s3_download_artifact_from_pipeline_job "${SOURCE_PATH}"

    # Replace original kcidb ids with ones matching the current pipeline
    # only keep matching build, the other jobs take care of the rest
    if is_debug_build; then
      BUILD_PACKAGE_NAME="${package_name%-debug}-debug"
    else
      BUILD_PACKAGE_NAME="${package_name}"
    fi
    KCIDB_CHECKOUT_ID_OLD=$(jq -r '.checkouts[].id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}")
    KCIDB_BUILD_ID_OLD_NOARCH=$(jq -r '.builds[] | select(.architecture == "noarch") | .id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}")
    KCIDB_BUILD_ID_NEW_NOARCH="${KCIDB_BUILD_ID_OLD_NOARCH/${KCIDB_CHECKOUT_ID_OLD}/${KCIDB_CHECKOUT_ID}}"
    # shellcheck disable=SC2016  # jq variable
    jq_i "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" \
      --arg kcidb_checkout_id "${KCIDB_CHECKOUT_ID}" \
      --arg kcidb_build_id "${KCIDB_BUILD_ID}" \
      --arg kcidb_build_id_noarch "${KCIDB_BUILD_ID_NEW_NOARCH}" \
      --arg build_arch "${ARCH_CONFIG}" \
      --arg build_package_name "${BUILD_PACKAGE_NAME}" \
      '
        .checkouts[].id = $kcidb_checkout_id |
        .builds |= map(
          select(
            .architecture == "noarch"
            or (.architecture == $build_arch and .misc.package_name == $build_package_name)
          ) |
          .checkout_id = $kcidb_checkout_id |
          .id = (
            if .architecture == "noarch" then
              $kcidb_build_id_noarch
            else
              $kcidb_build_id
            end
          )
        )
      '

    # Update important fields to match the *current* pipeline variables
    kcidb_checkout set-bool misc/retrigger "${CKI_RETRIGGER_PIPELINE}"
    kcidb_checkout set-bool misc/scratch "${scratch}"
    kcidb_checkout append-dict misc/provenance function="coordinator" url="${CI_PIPELINE_URL}" service_name="gitlab"
    kcidb_checkout set-time start_time now
    kcidb_build set-time start_time now
    kcidb build "${KCIDB_BUILD_ID_NEW_NOARCH}" set-time start_time now
  fi

.setup-generate-test-xml: |
  # 🎰 Generate the test XML using kpet.

  # If we have testing pipeline for a merge request, we need to pass the dummy
  # patch to kpet and targeted testing checker.
  # Don't do this if we're forcing baseline-like testing for MRs!
  if ! is_true "${force_baseline}" && [[ -n ${mr_id} ]]; then
    patch_urls="${CI_PROJECT_DIR}/${MR_DIFF_PATH}"
  fi

  if is_true "${test_upt}"; then
    # Generate the test XML to run UPT smoke tests.
    # shellcheck disable=SC2086 # warning: Double quote to prevent globbing and word splitting. [SC2086]
    read -r -a render_args <<< "$(join_by_multi ' --smoke-test ' ${upt_smoke_tests})"
    render_args+=(--arch "${ARCH_CONFIG}" --output "${BEAKER_TEMPLATE_PATH}")
    # Render the XML template for the UPT smoke tests.
    run_logged "${VENV_PY3}" -m upt.smoke_test.render "${render_args[@]}"
  else
    # Smoke-test the kpet module and kpet-db.
    # All invocations later on happen outside set -e.
    "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" test list > /dev/null

    # Get the path to the build URL, if applicable
    if ! is_true "${skip_kernel_installation}" ; then
      KERNEL_URL=$(get_output_file_link build kernel_package_url)
      if [ -z "${KERNEL_URL}" ] ; then
        cki_echo_error "Kernel URL is missing!"
        exit 1
      fi
    fi
    # Set the KERNEL_RELEASE environment variable so that it appears in the XML
    # output from KPET.
    export KERNEL_RELEASE
    KERNEL_RELEASE=$(kcidb_checkout get misc/kernel_version)
    # Set KPET_SETS to the default from trigger configuration, or to all sets
    KPET_SETS=${test_set:-.*}
    # Match user suffix "test-cki.suffix" against kpet set list
    _TMP_TEST_SET=$(echo "${KERNEL_RELEASE}" | grep -oP "test[-\.]cki\.\K(\w*)" || true)

    # The suffix has to match exactly
    _TMP_TEST_SET="$(run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" set list | cut -f1 -d " " | grep -w "${_TMP_TEST_SET}" || true)"
    if [ -n "${_TMP_TEST_SET}" ]; then
        KPET_SETS=${_TMP_TEST_SET}
    fi

    # Override test set when using AWS runner, as it can only run VM tests.
    if [ "${test_runner}" == "aws" ] ; then
      KPET_SETS="(${KPET_SETS}) & cloud"
    fi

    # Get zstream/ystream information about RHEL kernels
    if [ -n "${kpet_tree_name}" ]; then
      echo "Using kpet tree ${kpet_tree_name}"
    else
      kpet_tree_name=$("${VENV_PY3}" -m cki.cki_tools.select_kpet_tree \
        --package-name "${package_name}" \
        --kernel-version "${KERNEL_RELEASE}" \
        --nvr-tree-mapping-file "${SOFTWARE_DIR}/kpet-db/nvr-tree-mapping.yml" \
      )
      echo "Using kpet tree ${kpet_tree_name} determined from kernel version ${KERNEL_RELEASE}"
    fi
    kcidb_build set misc/kpet_tree_name "${kpet_tree_name}"

    # Generate a regular expression matching kpet-db's extra build components
    COMPONENTS_RE=""
    if ! is_true "${skip_kernel_installation}" ; then
      # If not testing a standalone package/image
      if ! [[ ${KERNEL_URL} =~ \.(rpm|tar\.gz)$ ]]; then
        # Generate a regex matching detected extra build components
        # (list packages, convert matching packages to component names, remove
        # duplicates, strip trailing newline, join lines with '|')
        # after the variant work is done, this can be cleaned up properly
        # shellcheck disable=SC2046 # FIXME warning disabled to enable linting: warning: Quote this to prevent word splitting. [SC2046]
        COMPONENTS_RE=$(paste -s -d'|' <<<$(
          loop dnf repoquery --disablerepo='*' --repofrompath "cki,${KERNEL_URL}" |
              awk '
                /^'"${package_name}"'-headers-/                {print "headers"}
                /^'"${package_name}"'-devel-/                  {print "devel"}
                /^'"${package_name}"'-debug-devel-/            {print "devel"}
                /^'"${package_name}"'-debuginfo-/              {print "debuginfo"}
                /^'"${package_name}"'-debug-debuginfo-/        {print "debuginfo"}
                /^'"${package_name}"'-abi-whitelists-/         {print "abi"}
                /^'"${package_name}"'-abi-stablelists-/        {print "abi"}
                /^'"${source_package_name}"'-headers-/         {print "headers"}
                /^'"${source_package_name}"'-devel-/           {print "devel"}
                /^'"${source_package_name}"'-debug-devel-/     {print "devel"}
                /^'"${source_package_name}"'-debuginfo-/       {print "debuginfo"}
                /^'"${source_package_name}"'-debug-debuginfo-/ {print "debuginfo"}
                /^'"${source_package_name}"'-abi-whitelists-/  {print "abi"}
                /^'"${source_package_name}"'-abi-stablelists-/ {print "abi"}
                /^perf-/                                       {print "tools"}
              ' | sort -u
        ))

        # If this is an official build, mark it as such. We can safely add "|officialbuild"
        # as official builds will always have a complete repo which matches some components
        # from the list above.
        if is_true "${officialbuild}"; then
          COMPONENTS_RE="${COMPONENTS_RE}${COMPONENTS_RE:+|}officialbuild"
        fi
      fi

      # If this is a non-tarball real-time build
      if [[ ${KERNEL_URL} != *.tar.gz ]] &&
         [[ ${package_name} == *-@(rt|automotive)* ]]; then
        COMPONENTS_RE="${COMPONENTS_RE}${COMPONENTS_RE:+|}rt"
      fi

      # If this is a debug build
      if is_debug_build; then
        COMPONENTS_RE="${COMPONENTS_RE}${COMPONENTS_RE:+|}debugbuild"
      fi
    fi
    # Add extra component regex from the pipeline variable, if specified
    if [ -n "${kpet_extra_components}" ]; then
      COMPONENTS_RE="${COMPONENTS_RE}${COMPONENTS_RE:+|}${kpet_extra_components}"
    fi
    echo "Generated build component regex for kpet: \"${COMPONENTS_RE}\""

    # Verify the architecture is supported with the stream
    # do not use grep -q as that will result in broken pipe errors
    if run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" arch list -t "${kpet_tree_name}" "${KPET_ARCH}" | grep . > /dev/null; then
      echo "${KPET_ARCH} is supported for ${kpet_tree_name}"
    else
      cki_echo_notify "${KPET_ARCH} is not supported for ${kpet_tree_name}"
      kcidb_build set misc/testing_skipped_reason "unsupported"
      kcidb_build set-bool misc/test_plan_missing false
      exit 0
    fi

    # Verify the debugbuild component is supported with the stream
    if is_debug_build; then
      # do not use grep -q as that will result in broken pipe errors
      if run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" component list -t "${kpet_tree_name}" -a "${KPET_ARCH}" | grep '^debugbuild' > /dev/null; then
        echo "debugbuild testing is supported for ${kpet_tree_name}/${KPET_ARCH}"
      else
        cki_echo_notify "debugbuild testing is not supported for ${kpet_tree_name}/${KPET_ARCH}"
        kcidb_build set misc/testing_skipped_reason "unsupported"
        kcidb_build set-bool misc/test_plan_missing false
        exit 0
      fi
    fi

    # Skip unsupported architectures we have to build to verify Brew builds don't break.
    # These are not supported by kpet and it gets very upset if we pass such values,
    # so we need to skip them for the multiarch command.
    mapfile -t architecture_array < <(
      "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" arch list -t "${kpet_tree_name}" \
        "$(sed -e 's/^ \+//; s/ \+$//; s/ \+/|/g' <<<"${architectures}")"
    )

    if ! is_true "${skip_kernel_installation}" ; then
      # Assemble parameters to add to the kernel URL for kpkginstall to use.
      # Always add source_package_name and package_name
      KERNEL_URL_PARAMS=(
        "package_name=${package_name}"
        "source_package_name=${source_package_name}"
      )
      # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
      # Add the debug_kernel parameter if we are testing a debug kernel.
      if is_true "${DEBUG_KERNEL}"; then
        KERNEL_URL_PARAMS+=("debug_kernel=true")
      fi

      # If we have parameters for the kernel URL, join them together and append
      # them to the kernel url.
      if [ ${#KERNEL_URL_PARAMS[@]} -gt 0 ]; then
        # The & -> &amp; conversion is required since valid XML cannot deal with
        # bare ampersands.
        JOINED_KERNEL_URL_PARAMS=$(join_by \& "${KERNEL_URL_PARAMS[@]}" | sed 's/&/&amp;/g')
        echo "Appending kernel URL parameters: ${JOINED_KERNEL_URL_PARAMS}"
        KERNEL_URL="${KERNEL_URL}#${JOINED_KERNEL_URL_PARAMS}"
        echo "New kernel URL: ${KERNEL_URL}"
      fi
    fi

    # Extract automotive configuration
    if [[ -n "${AUTOMOTIVE_CONFIGURATION_URL}" ]]; then
      automotive_config_release=$(echo "${AUTOMOTIVE_CONFIGURATION_URL}" | sed -E 's|.*/([^/]+)/[^/]+$|\1|')
      cki_echo_heading "Downloading automotive configuration from ${AUTOMOTIVE_CONFIGURATION_URL}"
      if ! curl \
        --config "${CKI_CURL_CONFIG_FILE}"  \
        --output "${AUTOMOTIVE_CONFIGURATION_PATH}" \
        "${AUTOMOTIVE_CONFIGURATION_URL}"; then
        cki_echo_error "  Unable to download configuration"
        exit 1
      fi
      # used by kpet_generate_args below
      echo "  automotive release: ${automotive_config_release}"
      beaker_xml="$(artifact_url "${BEAKER_TEMPLATE_PATH}")"
    fi

    # Ensure that _debug is added to the architecture string if we are testing a
    # debug kernel.
    # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
    if is_true "${DEBUG_KERNEL}"; then
      ARCH_STRING="${KPET_ARCH}_debug"
    else
      ARCH_STRING="${KPET_ARCH}"
    fi

    echo "KPET_SETS is: ${KPET_SETS}"

    declare kpet_variable_args kpet_noarch_args kpet_singlearch_args kpet_multiarch_args
    kpet_generate_args "${COMPONENTS_RE}" "${KPET_SETS}" "${architecture_array[@]}"

    if [ -z "${KPET_SETS}" ]; then
      echo "KPET_SETS is empty"
    else
      whiteboard=$(jq \
        --arg name "cki@gitlab:${CI_PIPELINE_ID} ${KERNEL_RELEASE}@${CI_COMMIT_REF_NAME} ${ARCH_STRING}" \
        '{"name": $name, "job_id": "GITLAB_JOB_ID_PLACEHOLDER"}' <<< "null"
      )

      # Run kpet to generate beaker XML.
      kpet_run_generate_command=(
        "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" run generate
        -v "job_group=cki"
        "${kpet_variable_args[@]}"
        -d "${whiteboard}"
      )
      if ! is_true "${skip_kernel_installation}" ; then
        kpet_run_generate_command+=(-k "${KERNEL_URL}")
        kpet_run_generate_command+=(-v selftests_url="$(get_output_file_link build selftests_url)")
      fi
      kpet_run_generate_command+=("${kpet_noarch_args[@]}" "${kpet_singlearch_args[@]}")

      # Quietly output unlinted XML for debugging, with "raw_" prefix
      # shellcheck disable=SC2001 # No, ${variable//search/replace} won't work
      "${kpet_run_generate_command[@]}" \
        --no-lint \
        -o "$(sed 's#^\(.*/\)\?#\1raw_#' <<<"${BEAKER_TEMPLATE_PATH}")" \
        2>/dev/null || true

      # Output actual XML
      run_logged "${kpet_run_generate_command[@]}" -o "${BEAKER_TEMPLATE_PATH}"
    fi
  fi
  beaker_xml="$(artifact_url "${BEAKER_TEMPLATE_PATH}")"
  cki_echo_notify "Generated test XML available in the job artifacts as ${beaker_xml}"
  kcidb_build append-dict output_files name=beaker_xml url="${beaker_xml}"

.setup-generate-test-plan: |
  # Generate test plan !
  if ! is_true "${skip_test}"; then
    "${VENV_PY3}" -m cki.kcidb.beaker_to_kcidb \
      --beaker-xml "${BEAKER_TEMPLATE_PATH}" \
      --kcidb-file "${KCIDB_DUMPFILE_NAME}" \
      --build-id "${KCIDB_BUILD_ID}" \
      --test-prefix "${KCIDB_TEST_PREFIX}"
    kcidb_build set-bool misc/test_plan_missing false
    kcidb_set_test_defaults
  fi

.setup-targeted: |
  # 🎯 Check whether this test run can have, and has any targeted tests/sources
  # If we have patches we can target
  if [ -n "${patch_urls:-}" ] ; then
    save_targeted_testing_info
  fi

# NOTE(mhayden): The `setup` stage allows us to potentially submit a job for
# testing in an external CI system while the normal CKI jobs run in the `test`
# stage.
.setup:
  extends: [.with_artifacts, .with_retries, .with_timeout, .with_python_image]
  stage: setup
  variables:
    ARTIFACTS: "artifacts/*.xml ${AUTOMOTIVE_CONFIGURATION_PATH}"
  # use stage dependencies instead of needs; this way, setup jobs only start
  # after _all_ previous jobs have finished successfully
  dependencies: []
  before_script:
    - !reference [.common-before-script]
  script:
    - !reference [.setup-prepare]
    - !reference [.setup-download-bot-artifacts]
    - !reference [.setup-generate-test-xml]
    - !reference [.setup-generate-test-plan]
    - !reference [.setup-targeted]
  after_script:
    - !reference [.common-after-script-head]
    - !reference [.common-after-script-tail]
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-setup-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

setup i686:
  extends: [.setup, .i686_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish i686

setup x86_64:
  extends: [.setup, .x86_64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish x86_64

setup x86_64 debug:
  extends: [.setup, .x86_64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish x86_64 debug

setup ppc64le:
  extends: [.setup, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish ppc64le

setup ppc64le debug:
  extends: [.setup, .ppc64le_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish ppc64le debug

setup aarch64:
  extends: [.setup, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish aarch64

setup aarch64 debug:
  extends: [.setup, .aarch64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish aarch64 debug

setup ppc64:
  extends: [.setup, .ppc64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish ppc64

setup s390x:
  extends: [.setup, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish s390x

setup s390x debug:
  extends: [.setup, .s390x_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish s390x debug

setup riscv64:
  extends: [.setup, .riscv64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish riscv64

setup riscv64 debug:
  extends: [.setup, .riscv64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish riscv64 debug
