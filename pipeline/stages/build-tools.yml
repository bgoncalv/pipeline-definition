---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.compile_tools: |
  # Natively compile the kernel tools.
  # obtain data from build job
  build_job_compiler="$(kcidb_build get compiler)"
  build_job_make_opts="$(kcidb_build get command)"
  build_job_build_time="$(kcidb_build get duration)"
  build_package_name="$(kcidb_build get misc/package_name)"
  # Allow easy handling of failed jobs by including all data.
  kcidb_build set compiler "${build_job_compiler} / $("${compiler}" --version | head -1)"
  dump_rpm_nvrs

  # Extract files from kernel-devel, if available. This is needed to compile
  # bpftool and selftests.
  kernel_version="$(kcidb_checkout get misc/kernel_version)"
  canonical_path="/usr/src/kernels/${kernel_version}.${ARCH_CONFIG}"
  cd /
    # Run the cpio command from the root directory so the files are placed into
    # the right spot right away because we can't get rid of the leading "./"
    rpm2cpio "${ARTIFACTS_DIR}/${build_package_name}-devel-${kernel_version}.${ARCH_CONFIG}.rpm" \
             | cpio -idmv ".${canonical_path}*/"{vmlinux.h,Module.symvers}
    # if files were extracted from variant kernel, move them to the "normal" location
    extracted_path=("${canonical_path}"*)
    if [[ -d "${extracted_path[0]}" ]] && \
       [[ "${extracted_path[0]}" != "${canonical_path}" ]]; then
      mv -v "${extracted_path[0]}" "${canonical_path}"
    fi
  cd "${CI_PROJECT_DIR}"

  RPMBUILD_WITHOUT_NATIVE_TOOLS_array=()
  create_array_from_string RPMBUILD_WITHOUT_NATIVE_TOOLS
  # Don't build any base kernels in tools jobs
  RPMBUILD_VARIANTS_array=()
  create_array_from_string RPMBUILD_VARIANTS
  RPMBUILD_WITHOUT_NATIVE_TOOLS_array+=("${RPMBUILD_VARIANTS_array[@]}")

  build_command=(rpmbuild --rebuild)
  for elt in "${RPMBUILD_WITHOUT_NATIVE_TOOLS_array[@]}"; do
      build_command+=(--without "${elt}")
  done

  if [[ "${compiler}" == 'clang' ]] ; then
    build_command+=(--with toolchain_clang)
  fi

  kcidb_build set command "${build_job_make_opts} / ${build_command[*]}"
  cki_echo_notify "Compiling the kernel tools with: ${build_command[*]}"

  # Update the log URL to contain tools build logs too
  kcidb_build set log_url "$(artifact_url "${BUILDLOG_PATH}")"

  store_time
  "${build_command[@]}" artifacts/*.src.rpm 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
  kcidb_build set-int duration "$((build_job_build_time + $(calculate_duration)))"

  # Move over needed artifacts.
  mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"
  cki_echo_heading "Kernel tools compiled successfully. Check the publish stage for full yum/dnf repository."

.build-tools:
  extends: [.with_artifacts, .with_retries, .with_timeout, .with_builder_image]
  stage: build-tools
  variables:
    # the publish stage expects exactly the same artifacts, with or without native tools
    ARTIFACTS: !reference [.build, variables, ARTIFACTS]
  before_script:
    - !reference [.common-before-script]
  script:
    - |
      kcidb_build append-dict misc/provenance function="executor" url="${CI_JOB_URL}" service_name="gitlab"
    - !reference [.build-prepare-rpm]
    - !reference [.compile_tools]
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        kcidb_build set-bool valid false
        print_and_save_build_failure "${KCIDB_BUILD_ID}"
      fi
    - !reference [.common-after-script-tail]
  rules:
    - !reference [.skip_without_native_tools]
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - !reference [.skip_with_native_architecture]
    - when: on_success

# only supported for >= RHEL8: ppc64le, aarch64, s390x

build_tools ppc64le:
  extends: [.build-tools, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder ppc64le
      build ppc64le
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder ppc64le}
    - {artifacts: false, job: build ppc64le}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-ppc64le

build_tools ppc64le debug:
  extends: [.build-tools, .ppc64le_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder ppc64le
      build ppc64le debug
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder ppc64le}
    - {artifacts: false, job: build ppc64le debug}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-ppc64le

build_tools aarch64:
  extends: [.build-tools, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder aarch64
      build aarch64
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder aarch64}
    - {artifacts: false, job: build aarch64}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-aarch64

build_tools aarch64 debug:
  extends: [.build-tools, .aarch64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder aarch64
      build aarch64 debug
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder aarch64}
    - {artifacts: false, job: build aarch64 debug}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-aarch64

build_tools s390x:
  extends: [.build-tools, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder s390x
      build s390x
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder s390x}
    - {artifacts: false, job: build s390x}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-s390x

build_tools s390x debug:
  extends: [.build-tools, .s390x_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder s390x
      build s390x debug
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder s390x}
    - {artifacts: false, job: build s390x debug}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-s390x

build_tools riscv64:
  extends: [.build-tools, .riscv64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder riscv64
      build riscv64
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder riscv64}
    - {artifacts: false, job: build riscv64}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-riscv64

build_tools riscv64 debug:
  extends: [.build-tools, .riscv64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder riscv64
      build riscv64 debug
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder riscv64}
    - {artifacts: false, job: build riscv64 debug}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-tools-runner-riscv64
